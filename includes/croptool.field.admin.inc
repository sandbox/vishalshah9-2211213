<?php

/**
 * @file
 * File for the Croptool functionality for nodes.
 */

/**
 * Implements function croptool_manage_fields().
 * 
 * Managing the tab fields.
 */
function croptool_manage_fields() {

  $field_disabled = "";
  drupal_add_library('system', 'ui.dialog');
  drupal_add_js(drupal_get_path('module', 'croptool') . '/js/croptool.js');
  drupal_add_js(drupal_get_path('module', 'croptool') . '/js/jquery.tablednd.js');
  $html = "";
  $rows = array();
  $form = array();
  if (arg(4) == "taxonomy") {
    $redirect_url = $GLOBALS['base_url'] . "/admin/config/content/croptool/taxonomy";
    $entity_type = "taxonomy_term";
  }
  if (arg(4) == "content-type") {
    $redirect_url = $GLOBALS['base_url'] . "/admin/config/content/croptool/content-type";
    $entity_type = "node";
  }

  $html .= '<fieldset id="edit-crop-tool" class="form-wrapper">';

  if (isset($_SESSION['selected_bundles'])) {
    if (@$_SESSION['selected_bundles'][1] != arg(4)) {
      $_SESSION['selected_bundles'][0] = 'select';
      $selected_content_type = 'select';
    }
    else {

      $selected_content_type = $_SESSION['selected_bundles'][0];
    }
    $header = array(
      array('data' => t('Field Name')),
      array('data' => t('Crop Configured?')),
      array('data' => t('Manage Fields')),
      array('data' => t('Auto Crop Images')),
    );
    $result = db_query("SELECT fi.field_name,fi.field_id FROM {field_config_instance} as fi 
            JOIN {field_config} as fc ON fi.field_id = fc.id AND fc.type = 'image'
            WHERE fi.bundle=:bundle", array('bundle' => $selected_content_type));
    if ($result->rowCount() > 0) {

      foreach ($result as $field) {
        $data = array();
        $crop_rows = array();
        $crop_status = 'no';
        $data[] = array("data" => $field->field_name);
        $field_id = $field->field_id;
        $crop_fields = db_query("SELECT * FROM {crop_field_details} f WHERE f.field_id = :field_id AND f.node_content_type=:node_content_type ORDER BY f.weight", array('field_id' => $field_id, 'node_content_type' => $selected_content_type));
        if ($crop_fields->rowCount() > 0) {
          $crop_header = array(array('data' => t('Tab Name')),
            array('data' => t('Field Name')),
            array('data' => t('Crop Enabled?')),
            array('data' => t('Edit')),
            array('data' => t('Delete')),
          );
          $crop_status = 'yes';
          foreach ($crop_fields as $crop_field) {
            $crop_data = array();
            $crop_data[] = array("data" => $crop_field->tab_name);
            $crop_data[] = array("data" => $crop_field->node_field_name);
            $crop_data[] = array("data" => $crop_field->crop_status);
            $crop_data[] = array("data" => '<a href="javascript:void(0);" onclick="loadCropDetails(' . $crop_field->id . ')">Edit</a>');
            $crop_data[] = array("data" => '<a href="javascript:void(0);" onclick="deleteCropTab(' . $crop_field->id . ')">Delete</a>');
            $crop_rows[] = array("data" => $crop_data, "id" => "t-" . $crop_field->id);
          }
          $html .= "<div id='div_crop_table_" . $field->field_id . "' style='display:none'>";
          $html .= theme('table', array(
            'header' => $crop_header,
            'rows' => $crop_rows,
            'attributes' => array("id" => "crop_table_" . $field->field_id),
            "sticky" => 0,
            )
          );
          $html .= "</div>";
        }

        $data[] = array("data" => $crop_status);
        $data[] = array("data" => '<a href="javascript:void(0);" onclick="openCroptoolDialog(0,' . $field->field_id . ')">Manage Crop Details</a>');
        $data[] = array("data" => '<a href="' . $GLOBALS['base_url'] . '/admin/config/content/croptool/auto-crop/' . $selected_content_type . '?field_id=' . $field->field_id . '&type=' . arg(4) . '">Auto Crop Images</a>');
        $rows[] = array("data" => $data);
      }
    }
    $html .= '<div id="dialog" style="display:none" title="Crop Configuration">';
    $html .= '<input type="hidden" id="hdnId" value="" />';
    $html .= '<input type="hidden" id="hdnfield_id" value="" />';
    $html .= '<input type="hidden" id="hdnnode_content_type" value="' . $selected_content_type . '" />';
    $html .= '<input type="hidden" id="hdn_redirect_url" value="' . $redirect_url . '" />';
    $html .= '<input type="hidden" id="hdn_base_url" value="' . $GLOBALS['base_url'] . '" />';
    $html .= '<input type="hidden" id="hdn_entity_type" value="' . $entity_type . '" />';
    $html .= '<table>';
    $html .= '<tr><th colspan="2">Note : All the fields are required</th></tr>';
    $html .= '<tr><td>Tab Name :</td><td><input type="text" class="form-text" id="txtTabName" /></td></tr>';
    $html .= '<tr><td>Field Name : </td><td><input type="text" class="form-text" id="txtFieldName" ' . $field_disabled . ' /></td></tr>';
    $html .= '<tr><td>Crop Height (Y) : </td><td><input type="text" class="form-text" id="txtCropHeight" /></td></tr>';
    $html .= '<tr><td>Crop Width (X): </td><td><input type="text" class="form-text" id="txtCropwidth" /></td></tr>';
    $html .= '<tr><td>Crop Enabled? : </td>';
    $html .= '<td><select id="cmbCropEnabled" class="form-select"><option value="yes">Yes</option><option value="no">No</option></select></td>';
    $html .= '</tr></table>';
    $html .= '<div id="loading" style="display:none;text-align: center;">';
    $html .= '<img height="24" width="24" src="' . $GLOBALS['base_url'] . '/' . drupal_get_path('module', 'croptool') . '/loader-trans.gif" />';
    $html .= '</div>';
    $html .= '<div class="cropTable" style="display:none"></div>';
    $html .= '</div>';
    drupal_add_js('jQuery(document).ready(function(){
    jQuery("#dialog").dialog({ modal: true, width: 500, resizable: false, autoOpen: false, buttons: {
    
    "Save": function() {
      saveCropDetails();   
    }, 
    "Save Order": function() {
      saveOrdering();   
    }, 
    "Reset": function() {
      clearFields();   
    },          
    Cancel: function() {
      var contentType = jQuery("#hdnnode_content_type").val();
      jQuery( this ).dialog( "close" );
      location.href = "' . $redirect_url . '?select_content_type="+contentType+"&action=cancel";
    },
    },                                                                
    open: function( event, ui ) { 
        
    }});
    console.log(jQuery("#dialog"));
    });', array('type' => 'inline'));

    if (count($rows) == 0) {
      $html .= "<span style='display:block;padding:0 15px 10px;'>" . t('No field available.') . "</span>";
    }
    else {
      $html .= theme(
        'table', array(
          'header' => $header,
          'rows' => $rows,
          'attributes' => array("id" => "tblArticles"),
          'sticky' => 0,
        )
      );
    }
  }
  else {

    $html .= "<span style='display:block;padding:0 15px 10px;'>" . t('No field available.') . "</span>";
  }

  $html .= '</fieldset>';

  $form = drupal_get_form('croptool_manage_fields_form');

  if (isset($_GET['action'])) {
    drupal_set_message(t('Croptool configuration successfully saved.'));
  }

  return drupal_render($form) . $html;
}

/**
 * Implements function to save fields.
 */
function croptool_manage_fields_save() {

  $dataarray = array();
  $dataarray = array(
    "field_id" => $_POST['fieldId'],
    "node_content_type" => $_POST['contentType'],
    "tab_name" => trim($_POST['txtTabName']),
    "crop_status" => $_POST['cmbCropEnabled'],
    "crop_height" => $_POST['txtCropHeight'],
    "crop_width" => $_POST['txtCropwidth']);
  $reg_exp_alphabetic = "/^[a-zA-Z0-9 ]+$/";
  if (preg_match($reg_exp_alphabetic, $dataarray["tab_name"]) == 0) {
    echo "Enter alphabetic value in tab name and field name.";
    exit;
  }
  if ($_POST['id'] != 0) {
    db_update('crop_field_details')
      ->fields($dataarray)
      ->condition('id', $_POST['id'], '=')
      ->execute();
  }
  else {
    $entity_type = trim($_POST['entityType']);
    $weight = db_query("SELECT MAX(f.weight) as max_weight FROM {crop_field_details} f WHERE f.field_id = :field_id AND node_content_type = :node_content_type", array('field_id' => $_POST['fieldId'], 'node_content_type' => $_POST['contentType']))->fetchAssoc();
    $dataarray['weight'] = $weight['max_weight'] + 1;
    $dataarray["node_field_name"] = strtolower(trim($_POST['txtFieldName']));
    $dataarray["node_field_name"] = str_replace(" ", "", $dataarray["node_field_name"]);
    db_insert('crop_field_details')
      ->fields($dataarray)
      ->execute();
    $field_name = strtolower(trim($_POST['txtFieldName']) . "_p");
    $field_name = str_replace(" ", "", $field_name);
    if (!field_info_field($field_name)) {

      $field = array(
        'field_name' => $field_name,
        'type' => 'field_hidden_text',
      );
      field_create_field($field);
      $instance = array(
        'field_name' => $field_name,
        'entity_type' => $entity_type,
        'label' => $field_name,
        'bundle' => trim($_POST['contentType']),
        'required' => FALSE,
        'widget' => array(
          'type' => 'field_hidden',
        ),
      );
      field_create_instance($instance);
    }
    $field_name = strtolower(trim($_POST['txtFieldName']) . "_c");
    $field_name = str_replace(" ", "", $field_name);
    if (!field_info_field($field_name)) {

      $field = array(
        'field_name' => $field_name,
        'type' => 'field_hidden_text',
      );
      field_create_field($field);
      $instance = array(
        'field_name' => $field_name,
        'entity_type' => $entity_type,
        'label' => $field_name,
        'bundle' => trim($_POST['contentType']),
        'required' => FALSE,
        'widget' => array(
          'type' => 'field_hidden',
        ),
      );
      field_create_instance($instance);
    }
  }
  echo "success";
  exit;
}

/**
 * Implements function to load fields values.
 */
function croptool_loadfields() {

  if (isset($_POST['contentType'])) {
    $content_type = $_POST['contentType'];
    $return = array();
    $result = db_query("SELECT f.field_id FROM {crop_field_details} f WHERE node_content_type = :node_content_type GROUP BY f.field_id", array('node_content_type' => $content_type));

    if ((isset($result) && $result->rowCount() > 0)) {
      foreach ($result as $row) {
        $field_info = field_info_field_by_id($row->field_id);
        $return[] = array("id" => $row->field_id, "name" => $field_info['field_name']);
      }
    }
    echo json_encode($return);
  }
  else {
    drupal_goto();
    return TRUE;
  }
}

/**
 * Implements function to load fields.
 */
function croptool_manage_fields_load() {
  if (!isset($_POST['id'])) {

    drupal_goto();
  }
  $id = $_POST['id'];

  $result = db_query("SELECT * FROM {crop_field_details} f WHERE f.id = :id", array('id' => $id));
  if ((isset($result) && $result->rowCount() > 0)) {
    foreach ($result as $row) {
      $returnarray['field_id'] = $row->field_id;
      $returnarray['node_content_type'] = $row->node_content_type;
      $returnarray['node_field_name'] = $row->node_field_name;
      $returnarray['tab_name'] = $row->tab_name;
      $returnarray['crop_status'] = $row->crop_status;
      $returnarray['crop_height'] = $row->crop_height;
      $returnarray['crop_width'] = $row->crop_width;
    }
  }
  else {
    $returnarray['field_id'] = '';
    $returnarray['node_content_type'] = '';
    $returnarray['node_field_name'] = '';
    $returnarray['tab_name'] = '';
    $returnarray['crop_status'] = '';
    $returnarray['crop_height'] = '';
    $returnarray['crop_width'] = '';
  }

  echo json_encode($returnarray);
}

/**
 * Implements function to check fields are exist.
 */
function croptool_check_field_existance() {

  $field_name_c = strtolower(trim($_POST['txtFieldName']) . "_c");
  $field_name_p = strtolower(trim($_POST['txtFieldName']) . "_p");
  if (!field_info_field($field_name_c) && !field_info_field($field_name_p)) {
    echo "0";
  }
  else {
    echo "1";
  }
  exit;
}

/**
 * Implements function to save weight.
 */
function croptool_save_weight() {
  if (!isset($_POST['order'])) {

    drupal_goto();
  }
  $order = $_POST['order'];
  $order = str_replace("t-", "", $order);
  $ids = explode(",", $order);
  foreach ($ids as $key => $id) {
    $dataarray = array();
    $dataarray = array("weight" => $key);
    db_update('crop_field_details')
      ->fields($dataarray)
      ->condition('id', $id, '=')
      ->execute();
  }
}

/**
 * Implements custom form to crop image.
 */
function croptool_crop_image_form($form, &$form_state) {

  drupal_add_library('system', 'ui.tabs');
  drupal_add_js(drupal_get_path('module', 'croptool') . '/js/croptool.js', array(), NULL, FALSE);

  $form = array();
  $nid = '';
  $title = '';
  $image = '';
  $image_js = '';
  $redirect = 'admin/config/content/node-listing';
  $crop_field_settings_arr = array();

  if (arg(5) != "") {
    $nid = arg(5);
    $field_id = $_GET['field_id'];
    $node = node_load($nid);
    $title = $node->title;
    $type = $node->type;
    $crop_tabs = db_query("SELECT * from {crop_field_details} f WHERE f.node_content_type = :node_content_type AND f.crop_status = :crop_status AND f.field_id = :field_id ORDER BY f.weight", array(
      'node_content_type' => $type,
      'crop_status' => 'yes',
      'field_id' => $field_id,
    ));
    $form['title'] = array(
      '#type' => 'item',
      '#markup' => '<b>' . $title . '</b>',
    );
    if ($crop_tabs->rowCount() > 0) {
      $i = 1;
      $tab_li = "";
      $tab_div = "";
      foreach ($crop_tabs as $crop_tab) {
        $crop_field_settings = array(
          "w" => $crop_tab->crop_width,
          'h' => $crop_tab->crop_height,
          'x' => '0',
          'y' => '0',
          'min_width' => $crop_tab->crop_width,
          'min_height' => $crop_tab->crop_height,
        );
        $image_field_name = db_query("SELECT * from {field_config_instance} f WHERE f.field_id = :field_id AND f.bundle = :bundle", array(
          'field_id' => $crop_tab->field_id,
          'bundle' => $type,
          ))->fetchAssoc();
        $image_field_name = $image_field_name['field_name'];
        $image_field_name = $node->$image_field_name;
        $image = (isset($image_field_name[$node->language][0]['uri']) ? $image_field_name[$node->language][0]['uri'] : '');
        if ($image == '') {
          $form['image'] = array(
            '#type' => 'item',
            '#markup' => 'No image available to crop.',
          );
          break;
        }
        $crop_field_c = $crop_tab->node_field_name . "_c";
        $crop_field_c_val = $node->$crop_field_c;
        $crop_field_c_val = (isset($crop_field_c_val[$node->language][0]['value']) ? $crop_field_c_val[$node->language][0]['value'] : '');
        if ($crop_field_c_val != "") {
          $crop_field_c_val_arr = unserialize($crop_field_c_val);
          $crop_field_c_val_arr['min_width'] = $crop_tab->crop_width;
          $crop_field_c_val_arr['min_height'] = $crop_tab->crop_height;
        }
        else {
          $crop_field_c_val_arr = $crop_field_settings;
        }
        $img_path = str_replace("public://", "", $image);
        $rt_path = $_SERVER['DOCUMENT_ROOT'] . base_path() . CROPTOOL_FILES_PATH;
        $img_info = getimagesize($rt_path . $img_path);
        $cp_dim = croptool_calculate_propotional($crop_tab->crop_width, $crop_tab->crop_height, $img_info[0], $img_info[1]);
        $form[$crop_tab->node_field_name . '_h'] = array(
          '#type' => 'hidden',
          '#default_value' => $crop_field_c_val_arr['h'],
          '#attributes' => array('id' => 'edit-image' . $i . '-height'),
        );
        $form[$crop_tab->node_field_name . '_w'] = array(
          '#type' => 'hidden',
          '#default_value' => $crop_field_c_val_arr['w'],
          '#attributes' => array('id' => 'edit-image' . $i . '-width'),
        );
        $form[$crop_tab->node_field_name . '_x'] = array(
          '#type' => 'hidden',
          '#default_value' => $crop_field_c_val_arr['x'],
          '#attributes' => array('id' => 'edit-image' . $i . '-x'),
        );
        $form[$crop_tab->node_field_name . '_y'] = array(
          '#type' => 'hidden',
          '#default_value' => $crop_field_c_val_arr['y'],
          '#attributes' => array('id' => 'edit-image' . $i . '-y'),
        );
        $form[$crop_tab->node_field_name . '_ph'] = array(
          '#type' => 'hidden',
          '#default_value' => $cp_dim['height'],
          '#attributes' => array('id' => 'edit-image' . $i . '-pheight'),
        );
        $form[$crop_tab->node_field_name . '_pw'] = array(
          '#type' => 'hidden',
          '#default_value' => $cp_dim['width'],
          '#attributes' => array('id' => 'edit-image' . $i . '-pwidth'),
        );

        $crop_field_settings_arr[$i] = $crop_field_c_val_arr;

        if ($image != "") {
          $image_to_show = $GLOBALS['base_url'] . "/" . CROPTOOL_FILES_PATH . str_replace("public://", "", $image);

          $tab_li .= '<li><a id="link-' . $i . '" href="#div-' . $i . '">' . $crop_tab->tab_name . '</a></li>';
          $tab_div .= '<div id="div-' . $i . '" style="position: relative;"><img id="img' . $i . '" src="' . $image_to_show . '" /><br /><br /><a href="javascript:void(0);" onclick="downloadImage(this)" id="' . $crop_tab->node_field_name . '">Download</a> | <a href="javascript:void(0);" onclick="resetSelection(' . $i . ');" id="reset-' . $i . '">Reset Selection</a></div>';
        }

        $i++;
      }
      if ($image != '') {
        $form['crop_field_settings_arr'] = array(
          '#type' => 'hidden',
          '#default_value' => json_encode($crop_field_settings_arr),
          '#attributes' => array('id' => 'crop_field_settings_arr'),
        );

        $form['image'] = array(
          '#type' => 'item',
          '#markup' => '<div id="tabs"><ul>' . $tab_li . '</ul>' . $tab_div . '</div>' . $image_js,
        );
        $form['nid'] = array(
          '#type' => 'hidden',
          '#value' => $nid,
        );
        $form['field_id'] = array(
          '#type' => 'hidden',
          '#value' => $field_id,
        );
        $form['download_field'] = array(
          '#type' => 'hidden',
          '#default_value' => "",
          '#attributes' => array('id' => 'download_field'),
        );
        $form['image_path'] = array(
          '#type' => 'hidden',
          '#value' => $image,
        );
        $form['submit'] = array(
          '#type' => 'submit',
          '#value' => 'Crop & Save',
          '#attributes' => array('id' => 'submit_crop_save'),
        );
      }
      else {
        $form['image'] = array(
          '#type' => 'item',
          '#markup' => 'No image available to crop.',
        );
      }
    }
    else {
      $form['image'] = array(
        '#type' => 'item',
        '#markup' => t('Crop details are not configured for image.'),
      );
    }
  }
  if (arg(7) != "" && arg(7) == "node") {
    $redirect = "node/" . $nid . "/edit";
  }
  $form['redirect_path'] = array(
    '#type' => 'hidden',
    '#value' => $redirect,
  );

  $form['cancel'] = array(
    '#type' => 'link',
    '#title' => 'Cancel',
    '#href' => $redirect,
  );

  $form['#attached']['library'][] = array('croptool', 'croptool');

  return $form;
}

/**
 * Implements submit function of cutom crop image form.
 */
function croptool_crop_image_form_submit(&$form, &$form_state) {

  $nid = $form_state['values']['nid'];
  $field_id = $form_state['values']['field_id'];

  $node = node_load($nid);
  $article_type = $node->type;
  $redirect_path = $form_state['values']['redirect_path'];
  $image_path = str_replace("public://", "", $form_state['values']['image_path']);
  $root_path = $_SERVER['DOCUMENT_ROOT'] . base_path() . CROPTOOL_FILES_PATH;

  $image_info = getimagesize($root_path . $image_path);
  $image_mime_type = $image_info['mime'];
  $image_create_path = $_SERVER['DOCUMENT_ROOT'] . base_path() . variable_get('file_croptool_path') . '/' . variable_get('croptool_crop_image_directory') . "/";

  $download_file_name = "";
  $filename = pathinfo($root_path . $image_path);
  $crop_tabs = db_query("SELECT * from {crop_field_details} f WHERE f.node_content_type = :node_content_type AND f.crop_status = :crop_status AND f.field_id = :field_id ORDER BY f.weight", array(
    'node_content_type' => $article_type,
    'crop_status' => 'yes',
    'field_id' => $field_id,
  ));
  if ($crop_tabs->rowCount() > 0) {

    foreach ($crop_tabs as $crop_tab) {
      $crop_field_data = array();
      $crop_field_data['c_h'] = $form_state['values'][$crop_tab->node_field_name . "_h"];
      $crop_field_data['c_w'] = $form_state['values'][$crop_tab->node_field_name . "_w"];
      $crop_field_data['c_x'] = $form_state['values'][$crop_tab->node_field_name . "_x"];
      $crop_field_data['c_y'] = $form_state['values'][$crop_tab->node_field_name . "_y"];
      $crop_field_data['c'] = serialize(array(
        "w" => $crop_field_data['c_w'],
        "h" => $crop_field_data['c_h'],
        "x" => $crop_field_data['c_x'],
        "y" => $crop_field_data['c_y'],
      ));
      $crop_field_data['crop_h'] = $crop_tab->crop_height;
      $crop_field_data['crop_w'] = $crop_tab->crop_width;
      $crop_field_data['crop_filename'] = $filename['filename'] . "_" . $crop_tab->node_field_name . "_" . $crop_field_data['crop_w'] . "_" . $crop_field_data['crop_h'] . "." . $filename['extension'];

      croptool_generate_images($image_mime_type, $root_path, $image_path, $crop_field_data, $image_create_path);

      $crop_field_c = $crop_tab->node_field_name . "_c";
      $crop_field_c_val = $node->$crop_field_c;
      $crop_field_c_val[$node->language][0]['value'] = $crop_field_data['c'];
      $node->$crop_field_c = $crop_field_c_val;
      if ($form_state['values']['download_field'] == $crop_tab->node_field_name) {
        $download_file_name = $crop_field_data['crop_filename'];
      }
      $crop_field_p = $crop_tab->node_field_name . "_p";
      $crop_field_p_val = $node->$crop_field_p;
      $crop_field_p_val[$node->language][0]['value'] = "public://field/image/" . $crop_field_data['crop_filename'];
      $node->$crop_field_p = $crop_field_p_val;
    }
  }

  node_save($node);

  if ($form_state['values']['download_field'] != "") {
    $dir = $image_create_path . $download_file_name;
    if (file_exists($dir)) {
      header('Pragma: public');
      header('Cache-Control: public, no-cache');
      header('Content-Type: application/octet-stream');
      header('Content-Length: ' . filesize($dir));
      header('Content-Disposition: attachment; filename="' . basename($dir) . '"');
      header('Content-Transfer-Encoding: binary');

      readfile($dir);
      exit;
    }
  }
  else {

    $form_state['redirect'] = $redirect_path;
  }
}

/**
 * Implements function croptool_manage_auto_crop_fields form.
 */
function croptool_manage_auto_crop_fields($form, &$form_state) {

  $crop_result = db_query("SELECT * FROM {crop_field_details} f WHERE f.node_content_type = :node_content_type AND f.field_id = :field_id ORDER BY f.weight", array(
    'node_content_type' => arg(5),
    'field_id' => $_REQUEST['field_id'],
  ));

  $crop_tabs = array();
  $articles_left = "";
  if (isset($_SESSION['cropped_articles']) && count($_SESSION['cropped_articles']) > 0) {
    $str = '<div class="messages status" style="height: 115px;overflow: auto;">';
    $str .= '<h2 class="element-invisible">' . t('Status message') . '</h2>';
    $str .= '<ul>';
    foreach ($_SESSION['cropped_articles'] as $message) {
      $str .= '<li>' . $message . '</li>';
    }
    $str .= '</ul>';
    $str .= '</div>';
    unset($_SESSION['cropped_articles']);
    $form['status-message'] = array(
      '#type' => 'item',
      '#markup' => $str,
    );
  }
  if ($_REQUEST['type'] == "taxonomy") {
    $action = $GLOBALS['base_url'] . "/admin/config/content/croptool/taxonomy";
    $crop_type = "terms";
  }
  elseif ($_REQUEST['type'] == "content-type") {
    $action = $GLOBALS['base_url'] . "/admin/config/content/croptool/content-type";
    $crop_type = "articles";
  }
  elseif ($_REQUEST['type'] == "user") {
    $action = $GLOBALS['base_url'] . "/admin/config/content/croptool/user";
    $crop_type = "user";
  }
  foreach ($crop_result as $crop) {

    $field_info = field_info_field_by_id($crop->field_id);
    $field_name = $crop->node_field_name . "_p_value";
    $field_name_table = "field_data_" . $crop->node_field_name . "_p";
    if ($_REQUEST['type'] == 'content-type') {

      $query = db_select('node', 'n');
      $query->addExpression("COUNT(n.nid)", 'article_count');
      $query->join('field_data_' . $field_info['field_name'], 'fi', 'fi.entity_id=n.nid');
      $query->leftJoin($field_name_table, 'f', 'f.entity_id = n.nid');
      $query->isNull('f.' . $field_name);
      $auto_crop_count_result = $query->condition('n.type', arg(5), '=')->execute()->fetchAssoc();
    }
    elseif ($_REQUEST['type'] == 'taxonomy') {
      $query = db_select('taxonomy_term_data', 't');
      $query->addExpression("COUNT(t.tid)", 'article_count');
      $query->join('field_data_' . $field_info['field_name'], 'fi', 'fi.entity_id=t.tid');
      $query->leftJoin($field_name_table, 'f', 'f.entity_id = t.tid');
      $query->isNull('f.' . $field_name);
      $auto_crop_count_result = $query->condition('fi.bundle', arg(5), '=')->execute()->fetchAssoc();
    }

    if ($auto_crop_count_result['article_count'] > 0) {
      $articles_left .= $auto_crop_count_result['article_count'] . " " . $crop_type . " left to be cropped for " . $crop->tab_name . " tab.<br />";
      $crop_tabs[$crop->id] = $crop->tab_name;
    }
  }
  if (count($crop_tabs) > 0) {
    $form['crop_tabs'] = array(
      '#type' => 'select',
      '#title' => t('Select Crop Tab'),
      '#options' => $crop_tabs,
      '#default_value' => '',
    );
    $form['crop_limit'] = array(
      '#type' => 'textfield',
      '#title' => t('Node/Term Limit'),
      '#required' => TRUE,
      '#description' => t('Enter number of node/term for which auto cropping is required.'),
      '#rules' => array('numeric'),
      '#element_validate' => array('element_validate_integer_positive'),
    );
    $form['content_type'] = array(
      '#type' => 'hidden',
      '#value' => arg(5),
      '#attributes' => array('id' => 'content_type'),
    );
    $form['crop_type'] = array(
      '#type' => 'hidden',
      '#value' => $_REQUEST['type'],
      '#attributes' => array('id' => 'crop_type'),
    );
    $form['field_id'] = array(
      '#type' => 'hidden',
      '#value' => $_REQUEST['field_id'],
      '#attributes' => array('id' => 'field_id'),
    );
    $form['title'] = array(
      '#type' => 'item',
      '#markup' => '<b>' . $articles_left . '</b>',
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => 'Crop Images',
    );
  }
  else {
    $form['image'] = array(
      '#type' => 'item',
      '#markup' => 'No ' . $crop_type . ' available to auto crop.',
    );
    $form['cancel'] = array(
      '#type' => 'item',
      '#markup' => "<a href='" . $action . "?select_content_type=" . arg(5) . "&action=cancel'>Cancel</a>",
    );
  }
  return $form;
}

/**
 * Implements function croptool_manage_auto_crop_fields_submit.
 */
function croptool_manage_auto_crop_fields_submit($form, &$form_state) {

  $crop_result = db_query("SELECT * FROM {crop_field_details} f WHERE f.id = :id", array('id' => $form_state['values']['crop_tabs']))->fetchAssoc();
  $field_info = field_info_field_by_id($crop_result['field_id']);
  $field_name = $crop_result['node_field_name'] . "_p_value";
  $field_name_table = "field_data_" . $crop_result['node_field_name'] . "_p";
  if ($_REQUEST['crop_type'] == 'content-type') {

    $query = db_select('node', 'n');
    $query->fields('n', array('nid'));
    $query->join('field_data_' . $field_info['field_name'], 'fi', 'fi.entity_id=n.nid');
    $query->leftJoin($field_name_table, 'f', 'f.entity_id = n.nid');
    $query->isNull('f.entity_id');
    $query->condition('n.type', $form_state['values']['content_type'], '=');
    $query->range(0, $form_state['values']['crop_limit']);
    $nodes = $query->execute();

    foreach ($nodes as $nid) {
      $node = node_load($nid->nid);
      $field_name = $field_info['field_name'];
      $field_name = $node->$field_name;
      $fid = $field_name[$node->language][0]['fid'];

      $return = croptool_autocrop_image($fid, $node->type);
      $field_name_p = $crop_result['node_field_name'] . "_p";
      $field_name_c = $crop_result['node_field_name'] . "_c";
      $field_name_p_val = $node->$field_name_p;
      $field_name_c_val = $node->$field_name_c;
      $field_name_c_val[$node->language][0] = array('value' => $return[$field_name_c]);
      $field_name_p_val[$node->language][0] = array('value' => $return[$field_name_p]);
      $node->$field_name_p = $field_name_p_val;
      $node->$field_name_c = $field_name_c_val;
      $messages[] = "<b>" . $node->title . "</b> has been updated for auto crop of <b>" . $crop_result['tab_name'] . "</b> crop tab.";
      node_save($node);
    }
  }
  elseif ($_REQUEST['crop_type'] == 'taxonomy') {

    $query = db_select('taxonomy_term_data', 't');
    $query->fields('t', array('tid'));
    $query->join('field_data_' . $field_info['field_name'], 'fi', 'fi.entity_id=t.tid');
    $query->leftJoin($field_name_table, 'f', 'f.entity_id = t.tid');
    $query->isNull('f.entity_id');
    $query->condition('fi.bundle', $form_state['values']['content_type'], '=');
    $query->range(0, $form_state['values']['crop_limit']);
    $terms = $query->execute();
    foreach ($terms as $tid) {

      $term = taxonomy_term_load($tid->tid);
      $field_name = $field_info['field_name'];
      $field_name = $term->$field_name;
      $fid = $field_name[LANGUAGE_NONE][0]['fid'];
      $return = croptool_autocrop_image($fid, $term->vocabulary_machine_name);
      $field_name_p = $crop_result['node_field_name'] . "_p";
      $field_name_c = $crop_result['node_field_name'] . "_c";
      $field_name_p_val = $term->$field_name_p;
      $field_name_c_val = $term->$field_name_c;
      $field_name_c_val[LANGUAGE_NONE][0] = array('value' => $return[$field_name_c]);
      $field_name_p_val[LANGUAGE_NONE][0] = array('value' => $return[$field_name_p]);
      $term->$field_name_p = $field_name_p_val;
      $term->$field_name_c = $field_name_c_val;
      $messages[] = "<b>" . $term->name . "</b> has been updated for auto crop of <b>" . $crop_result['tab_name'] . "</b> crop tab.";
      taxonomy_term_save($term);
    }
  }
  $_SESSION['cropped_articles'] = $messages;
}

/**
 * Implements function croptool_delete_tab Delete fields.
 */
function croptool_delete_tab() {

  if (!isset($_POST['id'])) {

    drupal_goto();
  }

  $id = $_POST['id'];
  $result = db_query("SELECT f.node_field_name FROM {crop_field_details} f WHERE f.id = :id", array('id' => $id))->fetchAssoc();

  field_delete_field($result['node_field_name'] . "_p");
  field_purge_batch();
  field_delete_field($result['node_field_name'] . "_c");
  field_purge_batch();

  db_delete('crop_field_details')
    ->condition('id', $id)
    ->execute();
}

/**
 * Implements function croptool_manage_fields_form.
 */
function croptool_manage_fields_form($form, &$form_state) {

  $form = array();

  if (arg(4) == "taxonomy") {

    $result = db_query("SELECT name,machine_name FROM {taxonomy_vocabulary}");
    $type_field = 'machine_name';
    $page_caption = t("Select taxonomy to configure crop tool");
    $entity_type = "Taxonomy";
  }
  if (arg(4) == "content-type") {

    $result = db_query("SELECT type,name FROM {node_type}");
    $type_field = 'type';
    $page_caption = t("Select content type to configure crop tool");
    $entity_type = "Content";
  }

  $option = array('select' => '-Select-');

  if ($result->rowCount() > 0) {
    foreach ($result as $type) {

      $option[$type->$type_field] = $type->name;
    }
  }

  if (@!in_array(arg(4), @$_SESSION['selected_bundles'])) {
    $_SESSION['selected_bundles'][0] = 'select';
  }

  $form['crop_tool'] = array(
    '#type' => 'fieldset',
    '#title' => check_plain('Crop Tool ' . $entity_type . ' Field Management'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['crop_tool']['crop_tool_select_option'] = array(
    '#type' => 'select',
    '#title' => check_plain($page_caption),
    '#options' => $option,
    '#default_value' => isset($_SESSION['selected_bundles'][0]) ? $_SESSION['selected_bundles'][0] : 'none',
    '#attributes' => array('onchange' => 'this.form.submit()'),
  );
  $form['crop_tool']['crop_tool_type'] = array(
    '#type' => 'hidden',
    '#value' => arg(4),
  );

  $form['go'] = array(
    '#type' => 'submit',
    '#value' => t('Go'),
    '#attributes' => array('style' => 'display: none;'),
  );
  return $form;
}

/**
 * Implements function croptool_manage_fields_form_submit.
 */
function croptool_manage_fields_form_submit($form, &$form_state) {

  $_SESSION['selected_bundles']['0'] = $form_state['values']['crop_tool_select_option'];
  $_SESSION['selected_bundles']['1'] = $form_state['values']['crop_tool_type'];
  drupal_set_message(t('The configuration options have been saved.'));
  return $form;
}
