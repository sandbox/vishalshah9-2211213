<?php

/**
 * @file
 * File for the Croptool functionality for TAXONOMY.
 */

/**
 * Implements function croptool_taxonomy_crop_image_form().
 */
function croptool_taxonomy_crop_image_form($form, &$form_submit) {
  global $base_url;
  drupal_add_library('system', 'ui.tabs');
  drupal_add_js(drupal_get_path('module', 'croptool') . '/js/croptool.js');
  $form = array();
  $tid = '';
  $title = '';
  $image = '';
  $image_js = '';
  $redirect = $base_url . 'admin/config/content/term-listing/';

  $crop_field_settings_arr = array();
  if (arg(5) != "") {
    $tid = arg(5);
    $field_id = $_GET['field_id'];
    $term = taxonomy_term_load($tid);
    $title = $term->name;
    $type = $term->vocabulary_machine_name;
    $crop_tabs = db_query("SELECT * from {crop_field_details} f WHERE f.node_content_type = :node_content_type AND f.crop_status = :crop_status AND f.field_id = :field_id ORDER BY f.weight", array(
      'node_content_type' => $type,
      'crop_status' => 'yes',
      'field_id' => $field_id,
    ));
    $form['title'] = array(
      '#type' => 'item',
      '#markup' => '<b>' . $title . '</b>',
    );
    if ($crop_tabs->rowCount() > 0) {
      $i = 1;
      $tab_li = "";
      $tab_div = "";
      foreach ($crop_tabs as $crop_tab) {
        $crop_field_settings = array(
          "w" => $crop_tab->crop_width,
          'h' => $crop_tab->crop_height,
          'x' => '0',
          'y' => '0',
          "min_width" => $crop_tab->crop_width,
          'min_height' => $crop_tab->crop_height,
        );
        $image_field_name = db_query("SELECT * from {field_config_instance} f WHERE f.field_id = :field_id AND f.bundle = :bundle", array(
          'field_id' => $crop_tab->field_id,
          'bundle' => $type,
          ))->fetchAssoc();
        $image_field_name = $image_field_name['field_name'];
        $image_field_name = $term->$image_field_name;
        $image = (isset($image_field_name[LANGUAGE_NONE][0]['uri']) ? $image_field_name[LANGUAGE_NONE][0]['uri'] : '');
        if ($image == '') {
          $form['image'] = array(
            '#type' => 'item',
            '#markup' => t('No image available to crop.'),
          );
          break;
        }
        $crop_field_c = $crop_tab->node_field_name . "_c";
        $crop_field_c_val = $term->$crop_field_c;
        $crop_field_c_val = (isset($crop_field_c_val[LANGUAGE_NONE][0]['value']) ? $crop_field_c_val[LANGUAGE_NONE][0]['value'] : '');

        if ($crop_field_c_val != "") {
          $crop_field_c_val_arr = unserialize($crop_field_c_val);
          $crop_field_c_val_arr['min_width'] = $crop_tab->crop_width;
          $crop_field_c_val_arr['min_height'] = $crop_tab->crop_height;
        }
        else {
          $crop_field_c_val_arr = $crop_field_settings;
        }
        $img_path = str_replace("public://", "", $image);
        $rt_path = $_SERVER['DOCUMENT_ROOT'] . base_path() . CROPTOOL_FILES_PATH;

        $img_info = getimagesize($rt_path . $img_path);
        $cp_dim = croptool_calculate_propotional($crop_tab->crop_width, $crop_tab->crop_height, $img_info[0], $img_info[1]);

        $form[$crop_tab->node_field_name . '_h'] = array(
          '#type' => 'hidden',
          '#default_value' => $crop_field_c_val_arr['h'],
          '#attributes' => array('id' => 'edit-image' . $i . '-height'),
        );
        $form[$crop_tab->node_field_name . '_w'] = array(
          '#type' => 'hidden',
          '#default_value' => $crop_field_c_val_arr['w'],
          '#attributes' => array('id' => 'edit-image' . $i . '-width'),
        );
        $form[$crop_tab->node_field_name . '_x'] = array(
          '#type' => 'hidden',
          '#default_value' => $crop_field_c_val_arr['x'],
          '#attributes' => array('id' => 'edit-image' . $i . '-x'),
        );
        $form[$crop_tab->node_field_name . '_y'] = array(
          '#type' => 'hidden',
          '#default_value' => $crop_field_c_val_arr['y'],
          '#attributes' => array('id' => 'edit-image' . $i . '-y'),
        );
        $form[$crop_tab->node_field_name . '_ph'] = array(
          '#type' => 'hidden',
          '#default_value' => $cp_dim['height'],
          '#attributes' => array('id' => 'edit-image' . $i . '-pheight'),
        );
        $form[$crop_tab->node_field_name . '_pw'] = array(
          '#type' => 'hidden',
          '#default_value' => $cp_dim['width'],
          '#attributes' => array('id' => 'edit-image' . $i . '-pwidth'),
        );
        $crop_field_settings_arr[$i] = $crop_field_c_val_arr;
        if ($image != "") {
          $image_to_show = $base_url . "/" . CROPTOOL_FILES_PATH . str_replace("public://", "", $image);
          $tab_li .= '<li><a id="link-' . $i . '" href="#div-' . $i . '">' . $crop_tab->tab_name . '</a></li>';
          $tab_div .= '<div id="div-' . $i . '" style="position: relative;"><img id="img' . $i . '" src="' . $image_to_show . '" /><br /><br /><a href="javascript:void(0);" onclick="downloadTaxonomyImage(this)" id="' . $crop_tab->node_field_name . '">Download</a> | <a href="javascript:void(0);" onclick="resetSelection(' . $i . ');" id="reset-' . $i . '">Reset Selection</a></div>';
        }
        $i++;
      }
      if ($image != '') {
        $form['crop_field_settings_arr'] = array(
          '#type' => 'hidden',
          '#default_value' => json_encode($crop_field_settings_arr),
          '#attributes' => array('id' => 'crop_field_settings_arr'),
        );
        $form['image'] = array(
          '#type' => 'item',
          '#markup' => '<div id="tabs"><ul>' . $tab_li . '</ul>' . $tab_div . '</div>' . $image_js,
        );
        $form['nid'] = array(
          '#type' => 'hidden',
          '#value' => $tid,
        );
        $form['tid'] = array(
          '#type' => 'hidden',
          '#value' => $tid,
        );
        $form['field_id'] = array(
          '#type' => 'hidden',
          '#value' => $field_id,
        );
        $form['download_field'] = array(
          '#type' => 'hidden',
          '#default_value' => "",
          '#attributes' => array('id' => 'download_field'),
        );
        $form['image_path'] = array(
          '#type' => 'hidden',
          '#value' => $image,
        );
        $form['submit'] = array(
          '#type' => 'submit',
          '#value' => t('Crop & Save'),
          '#attributes' => array('id' => 'submit_crop_save'),
        );
      }
      else {
        $form['image'] = array(
          '#type' => 'item',
          '#markup' => t('No image available to crop.'),
        );
      }
    }
    else {
      $form['image'] = array(
        '#type' => 'item',
        '#markup' => t('Crop details are not configured for image.'),
      );
    }
  }
  if (arg(7) != "" && arg(7) == "term") {
    $redirect = $base_url . "/taxonomy/term/" . $tid . "/edit";
  }
  $form['redirect_path'] = array(
    '#type' => 'hidden',
    '#value' => $redirect,
  );

  $form['cancel'] = array(
    '#type' => 'link',
    '#title' => t('Cancel'),
    '#href' => $redirect,
  );

  $form['#attached']['library'][] = array('croptool', 'croptool');

  return $form;
}

/**
 * Implements submit function of cutom crop image form.
 */
function croptool_taxonomy_crop_image_form_submit(&$form, &$form_state) {

  $tid = $form_state['values']['tid'];
  $field_id = $form_state['values']['field_id'];
  $term = taxonomy_term_load($tid);
  $article_type = $term->vocabulary_machine_name;
  $redirect_path = $form_state['values']['redirect_path'];
  $image_path = str_replace("public://", "", $form_state['values']['image_path']);
  $root_path = $_SERVER['DOCUMENT_ROOT'] . base_path() . CROPTOOL_FILES_PATH;
  $image_info = getimagesize($root_path . $image_path);
  $image_mime_type = $image_info['mime'];
  $image_create_path = $_SERVER['DOCUMENT_ROOT'] . base_path() . variable_get('file_croptool_path') . '/' . variable_get('croptool_crop_image_directory') . "/";

  $download_file_name = "";
  $filename = pathinfo($root_path . $image_path);
  $crop_tabs = db_query("SELECT * from {crop_field_details} f WHERE f.node_content_type = :node_content_type AND f.crop_status = :crop_status AND f.field_id = :field_id ORDER BY f.weight", array(
    'node_content_type' => $article_type,
    'crop_status' => 'yes',
    'field_id' => $field_id,
  ));

  if ($crop_tabs->rowCount() > 0) {
    foreach ($crop_tabs as $crop_tab) {
      $crop_field_data = array();

      $crop_field_data['c_h'] = $form_state['values'][$crop_tab->node_field_name . "_h"];
      $crop_field_data['c_w'] = $form_state['values'][$crop_tab->node_field_name . "_w"];
      $crop_field_data['c_x'] = $form_state['values'][$crop_tab->node_field_name . "_x"];
      $crop_field_data['c_y'] = $form_state['values'][$crop_tab->node_field_name . "_y"];
      $crop_field_data['c'] = serialize(array(
        "w" => $crop_field_data['c_w'],
        "h" => $crop_field_data['c_h'],
        "x" => $crop_field_data['c_x'],
        "y" => $crop_field_data['c_y'],
      ));
      $crop_field_data['crop_h'] = $crop_tab->crop_height;
      $crop_field_data['crop_w'] = $crop_tab->crop_width;
      $crop_field_data['crop_filename'] = $filename['filename'] . "_" . $crop_field_data['crop_w'] . "_" . $crop_field_data['crop_h'] . "." . $filename['extension'];

      croptool_generate_images($image_mime_type, $root_path, $image_path, $crop_field_data, $image_create_path);

      $crop_field_c = $crop_tab->node_field_name . "_c";
      $crop_field_c_val = $term->$crop_field_c;
      $crop_field_c_val[LANGUAGE_NONE][0]['value'] = $crop_field_data['c'];
      $term->$crop_field_c = $crop_field_c_val;

      if ($form_state['values']['download_field'] == $crop_tab->node_field_name) {
        $download_file_name = $crop_field_data['crop_filename'];
      }
      $crop_field_p = $crop_tab->node_field_name . "_p";
      $crop_field_p_val[LANGUAGE_NONE][0]['value'] = "public://field/" . variable_get('croptool_crop_image_directory') . "/" . $crop_field_data['crop_filename'];
      $term->$crop_field_p = $crop_field_p_val;
    }
  }

  taxonomy_term_save($term);

  if ($form_state['values']['download_field'] != "") {

    $dir = $image_create_path . $download_file_name;
    if (file_exists($dir)) {
      header('Pragma: public');
      header('Cache-Control: public, no-cache');
      header('Content-Type: application/octet-stream');
      header('Content-Length: ' . filesize($dir));
      header('Content-Disposition: attachment; filename="' . basename($dir) . '"');
      header('Content-Transfer-Encoding: binary');

      readfile($dir);
      exit;
    }
  }
  else {
    drupal_goto(url($redirect_path, array('absolute' => TRUE)));
  }
}

/**
 * Implements croptool_taxonomy_search.
 */
function croptool_taxonomy_search() {
  $keyword = trim(arg(2));

  $query = db_select('taxonomy_term_data', 't')->fields('t', array('tid', 'name'));
  $query->fields('cf', array('field_id'));
  $query->fields('v', array('machine_name'));
  $query->join('taxonomy_vocabulary', 'v', 'v.vid = t.vid');
  $query->join('crop_field_details', 'cf', 'cf.node_content_type = v.machine_name');

  $query->condition('cf.crop_status', 'yes', '=');

  if ($keyword != "nosearch") {
    $query->condition('t.name', '%' . $keyword . '%', "like");
  }
  $query->orderBy('tid', 'DESC');
  $query->distinct();
  $query = $query->extend('TableSort')->extend('PagerDefault')->limit(10);
  $result = $query->execute();
  $items = array();
  foreach ($result as $term) {
    $crop_result = db_query("SELECT f.field_id,count(DISTINCT(f.field_id)) as count_field_id FROM {crop_field_details} f WHERE f.node_content_type = :node_content_type GROUP BY f.node_content_type", array('node_content_type' => $term->machine_name))->fetchAssoc();
    $items[$term->tid] = array(
      "title" => $term->name,
      "type" => $term->machine_name,
      "field_count" => $crop_result['count_field_id'],
      "field_id" => $crop_result['field_id'],
    );
  }
  $page_html = '';
  if (empty($items)) {
    $page_html = '<p>No Terms found.</p>';
  }
  else {

    $header = array(array('data' => t('Term Id')),
      array('data' => t('Name')),
      array('data' => ''),
    );

    foreach ($items as $key => $value) {
      $edit_path = url('admin/config/content/croptool-taxonomy/edit/' . $key, array('absolute' => TRUE));
      $data = array();
      $data[] = array("data" => $key);
      $data[] = array("data" => addslashes($value['title']));
      if ($value['field_count'] > 1) {

        $data[] = array("data" => '<a href="javascript:void(0);" onclick="openCropFieldsDialog(\'' . $value['type'] . '\',' . $key . ')">Crop Image</a>');
      }
      else {
        $data[] = array("data" => '<a href="' . $edit_path . '?field_id=' . $value['field_id'] . '">Crop Image</a>');
      }
      $rows[] = array("data" => $data);
    }
    $page_html .= theme('table', array(
      'header' => $header,
      'rows' => $rows,
      "sticky" => 0,
    ));
    $page_html .= theme('pager');
    $page_html .= '</div>';
  }

  echo $page_html;
  exit;
}
