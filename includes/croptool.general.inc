<?php
/**
 * @file
 * File for the managing generic configurations Croptool module.
 */


/**
 * Implements call croptool_setting_form().
 */
function croptool_setting() {
  $form = drupal_get_form('croptool_setting_form');
  return drupal_render($form);
}
/**
 * Implements function croptool_setting_form().
 */
function croptool_setting_form($form, &$form_state) {

  $form['crop_tool'] = array(
    '#type' => 'fieldset',
    '#title' => t('Crop Tool Configuration'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['crop_tool']['crop_image_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Crop Images path'),
    '#required' => TRUE,
    '#description' => t('Path to uploaded cropped images relative to the document root. <br/> Note: The filename cannot contain any type following characters \ / : * ? " < > |'),
    '#default_value' => variable_get('croptool_crop_image_directory'),
  );
  $form['crop_tool']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}
/**
 * Implements function croptool_setting_form_validate().
 */
function croptool_setting_form_validate($form, &$form_state) {
  $rt_path = $_SERVER['DOCUMENT_ROOT'] . base_path() . variable_get('file_croptool_path') . '/' . $form_state['values']['crop_image_path'];
  file_prepare_directory($rt_path, FILE_CREATE_DIRECTORY);

  if (!file_prepare_directory($rt_path, FILE_CREATE_DIRECTORY)) {

    form_set_error('crop_image_path', t('The directory could not be created.'));
  }
  return $form;
}
/**
 * Implements function croptool_setting_form_submit().
 */
function croptool_setting_form_submit($form, &$form_state) {
  $directory = rtrim($form_state['values']['crop_image_path'], '/\\');
  variable_set('croptool_crop_image_directory', $directory);

  drupal_set_message(t('The configuration options have been saved.'));
  return $form;
}
