(Drupal Image Croptool module)

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Configuration
 * Crop tab creation for nodes/taxonomy
 * Crop image for node/term
 * Auto cropping of images
 * Fronted usage
 * Requirements
 * Permissions
 * Installation and un-installation

INTRODUCTION
------------

Maintainer: 
Khyati Yagnik <khyati.yagnik@clariontechnologies.co.in>
Rahul Nahar <rahul.nahar@clariontechnologies.co.in>

This module provides facility to crop single image in multiple dimensions.
User can crop various part of singe image based on dimensions configured from
back-end.Path for each cropped image is stored in one hidden field,we can fetch
each cropped image for node/term using the hidden fields.In this way 
we can display different portions of image at each place holder in fronted.
 
CONFIGURATION
---------------

In admin one configuration page is provided to configure default 
location for storing cropped images on server.

CROP TAB CREATION FOR NODES/TAXONOMY
-------------------------------

This module provides support for content types and taxonomy.
User can create crop tabs for each content type/taxonomy.
Crop tab will be created for image fields of content type/taxonomy.
After enabling this module,
following links will be available at Admin->Configuration
1. Crop Tool Content Tabs
2. Crop Tool Taxonomy Tabs

On this page user can create crop tab by following steps
1. Select content type/taxonomy
2. All image fields for selected type will be displayed.
   Click on "Manage crop details" link.
3. Fill up the form displayed to configure crop dimensions and crop tab name.

This module will create hidden fields to store cropped image- 
path and coordinates for each crop tab created using above steps. 


CROP IMAGE FOR NODE/TERM
---------------------

User can crop image for particular field from node/term edit form.
In that form crop tool tab will be comnig up along with link to cropping page
based on fields for which crop tabs are created.
On the cropping page all tabs created for that particular field
will be displayed and user can crop image from this page.

AUTO CROPPING OF IMAGES
-----------------------

User can also auto crop images in bulk from following pages for nodes/terms
1. Crop Tool Content Tabs
2. Crop Tool Taxonomy Tabs

On these pages "Auto Crop Images" link will be available after
selecting content type/taxonomy.
From this section user can select filed for which 
he/she can auto crop uploaded images for nodes/terms.

FRONTED USAGE
-------------

For each crop dimension/tab two hidden fields are generated for node
to store cropped image location and dimension. Benefit of
this approach is that,in frontend we can render cropped image
with required dimensions at various location.

REQUIREMENTS
------------

 * Field Hidden
 * Libraries
 * jQuery imgareaselect plugin
 
INSTALLATION AND UNINSTALLATION
-------------------------------

Ordinary installation and unstallation.

Introduces persistent variable; these are automatically deleted when
uninstalling.

Creates new table for croptool configuration on installation.
On installation croptool configuration table will be dropped from database.
All the fields created by module will also be deleted.

PERMISSIONS
-----------

User having "Access Administration Page" can only configure crop tool.
