<?php

/**
 * @file
 * Install, update and uninstall functions for the Drupal Crop Tool module.
 * Implements hook_schema().
 *
 * Generate the current version of the database schema from
 * the sequence of schema update functions. Uses a similar
 * method to install.inc's drupal_get_schema_versions() to
 * establish the update sequence.
 *
 * To change the schema, add a new views_schema_N()
 * function to match the associated views_update_N()
 *
 * @param $caller_function
 *   The name of the function that called us.
 *   Used internally, if requesting a specific schema version.
 */

/**
 * Implements hook_requirements().
 */
function croptool_requirements($phase) {
  $requirements = array();

  if ($phase == 'install') {
    $t = get_t();
    $path = '';
    if (module_exists('libraries')) {
      $path = libraries_get_path('jquery.imgareaselect');
    }
    if ($path == '') {
      $path = '/sites/all/libraries/jquery.imgareaselect';
    }
    if (file_exists($path . '/scripts/jquery.imgareaselect.min.js')) {
      $requirements['field_image_crop_plugin'] = array(
        'title' => $t('JQuery Imgareaselect plugin'),
        'severity' => REQUIREMENT_OK,
        'value' => $t('Installed'),
      );
    }
    else {
      $filelocation = $path . '/scripts/jquery.imgareaselect.min.js';
      $requirements['field_image_crop_plugin'] = array(
        'title' => $t('JQuery Imgareaselect plugin'),
        'value' => $t('Not found'),
        'severity' => REQUIREMENT_ERROR,
        'description' => $t('You need to download the !name and move the downloaded js file(s) into the %path folder of your server and file location is %filelocation.', array(
          '!name' => l($t('JQuery Imgareaselect plugin'), 'http://odyniec.net/projects/imgareaselect'),
          '%path' => $path,
          '%filelocation' => $filelocation,
        )),
      );
    }
  }
  return $requirements;
}

/**
 * Force clear cache.This is needed to register some new template files.
 */
function croptool_update_7001() {
  cache_clear_all();
}

/**
 * Implements hook_schema().
 */
function croptool_schema() {
  $schema['crop_field_details'] = array(
    'description' => 'Crop Field Details',
    'fields' => array(
      'id' => array(
        'description' => 'My unique identifier',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'field_id' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'node_content_type' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'node_field_name' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'tab_name' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'crop_status' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'crop_height' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'crop_width' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'weight' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('id'),
  );

  return $schema;
}

/**
 * Implements hook_install().
 */
function croptool_install() {
  $files_dir = file_stream_wrapper_get_instance_by_uri('public://')->getDirectoryPath();
  variable_set('file_croptool_path', $files_dir);
  variable_set('croptool_crop_image_directory', 'cropimages');
  $rt_path = $_SERVER['DOCUMENT_ROOT'] . base_path() . variable_get('file_croptool_path') . '/cropimages';
  file_prepare_directory($rt_path, FILE_CREATE_DIRECTORY);
}

/**
 * Implements hook_uninstall().
 */
function croptool_uninstall() {
  variable_del('croptool_crop_image_directory');
  $query = db_query("SELECT node_field_name from {crop_field_details}");
  if ((isset($query) && $query->rowCount() > 0)) {
    foreach ($query as $row) {
      field_delete_field($row->node_field_name . "_p");
      field_purge_batch(1);
      field_delete_field($row->node_field_name . "_c");
      field_purge_batch(1);
    }
  }
}
