/**
 * @file
 * Drupal Crop Tool module administration js functions
 */
var $img = new Array();
function openCroptoolDialog(id, field_id)
{
  clearFields();
  jQuery("#hdnfield_id").val(field_id);
  jQuery("#hdnId").val(id);
  if (id != 0) {
    loadCropDetails(id);
  }
  jQuery(".cropTable").html(jQuery("#div_crop_table_" + field_id).html());
  if (jQuery(".cropTable #crop_table_" + field_id).length > 0) {
    jQuery(".cropTable #crop_table_" + field_id).tableDnD();
    jQuery(".cropTable").css("height", "200px");
    jQuery(".cropTable").css("overflow", "auto");
    jQuery(".cropTable").show();
  }

  jQuery("#dialog").dialog('open');
}
function clearFields() {
  jQuery("#hdnId").val(0);
  jQuery("#txtFieldName").removeAttr("disabled");
  jQuery("#txtTabName").val("");
  jQuery("#txtFieldName").val("");
  jQuery("#txtCropHeight").val("");
  jQuery("#txtCropwidth").val("");
  jQuery("#cmbCropEnabled").val("");
}

function loadCropDetails(id) {
  var baseUrl = jQuery("#hdn_base_url").val();
  jQuery("#hdnId").val(id);
  jQuery.ajax({
    type: 'POST',
    url: baseUrl + '/admin/config/content/croptool/loadcropdetails',
    data: {'id': id},
    success: function(data) {
      jQuery("#txtTabName").val(data.tab_name);
      jQuery("#txtFieldName").val(data.node_field_name);
      jQuery("#txtCropHeight").val(data.crop_height);
      jQuery("#txtCropwidth").val(data.crop_width);
      jQuery("#cmbCropEnabled").val(data.crop_status);
      jQuery("#txtFieldName").attr("disabled", "true");
      jQuery("#dialog").dialog('open');
    },
    dataType: 'json'
  });
}

function saveCropDetails()
{

  var fieldId = jQuery("#hdnfield_id").val();
  var contentType = jQuery("#hdnnode_content_type").val();
  var redirectUrl = jQuery("#hdn_redirect_url").val();
  var id = jQuery("#hdnId").val();
  var txtTabName = jQuery.trim(jQuery("#txtTabName").val());
  var txtFieldName = jQuery("#txtFieldName").val();
  var txtCropHeight = jQuery("#txtCropHeight").val();
  var txtCropwidth = jQuery("#txtCropwidth").val();
  var cmbCropEnabled = jQuery("#cmbCropEnabled").val();
  var entityType = jQuery("#hdn_entity_type").val();
  var baseUrl = jQuery("#hdn_base_url").val();

  if (!(/^[0-9]+$/.test(txtCropHeight))) {
    alert("Enter numeric value in crop height field.");
    return false;
  } else if (parseInt(txtCropHeight) <= 0 || parseInt(txtCropHeight) < 20 || parseInt(txtCropHeight) > 1024) {
    alert("Enter numeric value greater than 20 and less than 1024 in crop height field.");
    return false;
  }
  if (!(/^[0-9]+$/.test(txtCropwidth))) {
    alert("Enter numeric value in crop width field.");
    return false;
  } else if (parseInt(txtCropwidth) <= 0 || parseInt(txtCropwidth) < 20 || parseInt(txtCropwidth) > 1024) {
    alert("Enter numeric value greater than 20 and less than 1024 in crop width field.");
    return false;
  }

  if (id == 0) {
    jQuery.ajax({
      type: 'POST',
      async: false,
      url: baseUrl + '/admin/config/content/croptool/checkfieldexistance',
      data: {'txtFieldName': txtFieldName},
      success: function(data) {
        if (data == "1") {
          alert("Field with this name already exist.");
          return false;
        } else {
          jQuery("#loading").show();
          jQuery.ajax({
            type: 'POST',
            async: false,
            url: baseUrl + '/admin/config/content/croptool/save',
            data: {'id': id, 'fieldId': fieldId, 'contentType': contentType, 'txtTabName': txtTabName, 'txtFieldName': txtFieldName, 'cmbCropEnabled': cmbCropEnabled, 'txtCropHeight': txtCropHeight, 'txtCropwidth': txtCropwidth, 'entityType': entityType},
            success: function(data) {
              if (data == "success") {
                jQuery("#dialog").dialog('close');
                location.href = redirectUrl + "?select_content_type=" + contentType + "&action=save";
              }
              else {
                alert(data);
                jQuery("#loading").hide();
              }
            }
          });
        }
      },
    });
  } else {
    jQuery("#loading").show();
    jQuery.ajax({
      type: 'POST',
      async: false,
      url: baseUrl + '/admin/config/content/croptool/save',
      data: {'id': id, 'fieldId': fieldId, 'contentType': contentType, 'txtTabName': txtTabName, 'txtFieldName': txtFieldName, 'cmbCropEnabled': cmbCropEnabled, 'txtCropHeight': txtCropHeight, 'txtCropwidth': txtCropwidth},
      success: function(data) {
        if (data == "success") {
          jQuery("#dialog").dialog('close');
          location.href = redirectUrl + "?select_content_type=" + contentType + "&action=save";
        }
        else {
          alert(data);
          jQuery("#loading").hide();
        }
      }
    });
  }
}

function saveOrdering() {
  var redirectUrl = jQuery("#hdn_redirect_url").val();
  var baseUrl = jQuery("#hdn_base_url").val();
  var fieldId = jQuery("#hdnfield_id").val();
  var contentType = jQuery("#hdnnode_content_type").val();
  jQuery(".cropTable #crop_table_" + fieldId)
  var allRow = jQuery(".cropTable #crop_table_" + fieldId + " tbody tr");

  if (allRow.length == 0) {
    alert("No rows available to order.");
    return;
  }

  var str = '';
  for (i = 0; i < allRow.length; i++) {
    str += jQuery(allRow[i]).attr("id") + ",";
  }
  str = str.substr(0, str.length - 1);
  jQuery.ajax({
    url: baseUrl + "/admin/config/content/croptool/save-weight",
    type: "POST",
    data: "order=" + str,
    success: function(data) {
      jQuery("#dialog").dialog('close');
      location.href = redirectUrl + "?select_content_type=" + contentType + "&action=save";
    },
  });
}

function initializeCropBox(field_settings) {
  var crop_opts = {};
  crop_opts["minWidth"] = field_settings.min_width;
  crop_opts["minHeight"] = field_settings.min_height;
  crop_opts["maxWidth"] = "";
  crop_opts["maxHeight"] = "";
  crop_opts["handles"] = true;
  crop_opts["onSelectEnd"] = preview;
  crop_opts["onSelectChange"] = preview;
  crop_opts["instance"] = true;
  crop_opts["aspectRatio"] = field_settings.min_width + ":" + field_settings.min_height;
  crop_opts["persistent"] = true;

  if (field_settings.w != "") {
    crop_opts["width"] = parseInt(field_settings.w);
  }
  if (field_settings.h != "") {
    crop_opts["height"] = parseInt(field_settings.h);
  }
  if (field_settings.x != "") {
    crop_opts["x1"] = parseInt(field_settings.x);
    crop_opts["x2"] = parseInt(field_settings.x) + parseInt(field_settings.w);
  }

  if (field_settings.y != "") {
    crop_opts["y1"] = parseInt(field_settings.y);
    crop_opts["y2"] = parseInt(field_settings.y) + parseInt(field_settings.h);
  }
  return crop_opts;
}

function initializeTabs($img, tab_index, tab_count) {
  for (j = 1; j <= tab_count; j++) {
    if (j == tab_index) {
      $img[j].setOptions({show: true, enable: true});
      $img[j].update();
    }
    else {
      $img[j].setOptions({hide: true, disable: true});
      $img[j].update();
    }
  }
}

function preview(img, selection) {
  var id = $(img).attr("id");
  var called = id.substr(3);
  $("#edit-image" + called + "-height").val(selection.height);
  $("#edit-image" + called + "-width").val(selection.width);
  $("#edit-image" + called + "-x").val(selection.x1);
  $("#edit-image" + called + "-y").val(selection.y1);
}
function downloadImage(elm) {
  jQuery("#download_field").val(jQuery(elm).attr("id"));
  jQuery("#croptool-crop-image-form").submit();
}

function downloadTaxonomyImage(elm) {
  jQuery("#download_field").val(jQuery(elm).attr("id"));
  jQuery("#croptool-taxonomy-crop-image-form").submit();
}

function openCropFieldsDialog(contentType, nodeId) {
  var baseUrl = jQuery("#hdn_base_url").val();
  var html = "";
  var url = jQuery("#edit-path").val() + nodeId;

  jQuery.ajax({
    type: 'POST',
    url: baseUrl + '/admin/config/content/croptool/loadfields',
    data: {'contentType': contentType},
    success: function(data) {

      html = "";
      jQuery.each(data, function(index, value) {
        html += "<a href='" + url + "?field_id=" + value.id + "'>" + value.name + "</a>";
        html += "<br />";
      });
      jQuery("#dialog").html(html);
      jQuery("#dialog").dialog('open');
    },
    dataType: 'json'
  });

}


function bindClickTopager(page) {
  var links = jQuery('.pager li');
  if (links.length > 0) {
    for (i = 0; i < links.length; i++) {
      var a = links[i].childNodes;
      var lnk = jQuery(a[0]).attr("href");
      if (lnk != null) {
        jQuery(a[0]).click(function() {
          var lnk = jQuery(this).attr("href");

          if (lnk.indexOf("croptool") > 0) {
            pagerUrl = lnk;
          }
          else {
            var pageNo = lnk.substring(lnk.indexOf("=") + 1, lnk.length);
            pagerUrl = page + "nosearch?page=" + pageNo;
          }
          jQuery.ajax({
            url: pagerUrl,
            type: "POST",
            success: function(data, textStatus, jqXHR) {
              jQuery("#articleResults").html(data);
              bindClickTopager(page);
            }
          });
          return false;
        });

      }
    }
  }
}
jQuery(document).ready(function() {
  $ = jQuery.noConflict();
  if (jQuery("#submit_crop_save").length > 0) {
    jQuery("#submit_crop_save").click(function() {
      jQuery("#download_field").val("");
    });
  }
  if (jQuery("#txtTabName").length > 0) {

    jQuery("#txtTabName").blur(function() {
      if (jQuery("#hdnId").val() == 0) {
        var fieldName = jQuery("#txtTabName").val();
        jQuery("#txtFieldName").val(fieldName);
      }
    });
  }
  if (jQuery("#crop_field_settings_arr").length > 0) {
    jQuery("#tabs").tabs();
    var arr = JSON.parse(jQuery("#crop_field_settings_arr").val());

    var arr_length = Object.keys(arr).length;
    window.setTimeout(function() {
      var firstTime = true;
      for (var k = 1; k <= arr_length; k++) {
        var opts = initializeCropBox(arr[k]);
        $img[k] = $("#img" + k).imgAreaSelect(opts);
      }

      for (var i = 1; i <= arr_length; i++) {
        if (i != 1) {
          $img[i].setOptions({hide: true, disable: true});
          $img[i].update();
        }
        $("#link-" + i).click(function() {
          var tabIndex = jQuery(this).attr("id").replace("link-", "");
          initializeTabs($img, tabIndex, arr_length);
          if (firstTime) {
            window.setTimeout(function() {
              initializeTabs($img, tabIndex, arr_length);
            }, 100);
            firstTime = false;
          }

        });
      }
    }, 700);
  }
});

Object.keys = Object.keys || function(o) {
  var result = [];
  for (var name in o) {
    if (o.hasOwnProperty(name)) {
      result.push(name);
    }
  }
  return result;
};

function resetSelection(tab) {
  var a_h = jQuery("#edit-image" + tab + "-pheight").val();
  var a_w = jQuery("#edit-image" + tab + "-pwidth").val();

  $img[tab].setSelection(0, 0, a_w, a_h);
  $img[tab].update();
}

function deleteCropTab(id) {
  var baseUrl = jQuery("#hdn_base_url").val();
  var redirectUrl = jQuery("#hdn_redirect_url").val();
  var contentType = jQuery("#hdnnode_content_type").val();
  jQuery("#loading").show();
  jQuery.ajax({
    type: 'POST',
    async: false,
    url: baseUrl + '/admin/config/content/croptool/deletecroptab',
    data: {'id': id},
    success: function(data) {
      jQuery("#dialog").dialog('close');
      location.href = redirectUrl + "?select_content_type=" + contentType + "&action=save";
    },
  });
}
